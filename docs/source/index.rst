.. pop-create-idem documentation master file, created by
   sphinx-quickstart on Mon Jun 25 20:19:13 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pop-create-idem's Documentation!
===========================================

This project is an app-merge component for `pop-create`.
It's purpose is to make all the boilerplate code for an idem-cloud project.


.. toctree::
   :maxdepth: 2
   :glob:

   tutorial/quickstart
   tutorial/migrate_salt_cloud
   tutorial/idem_cloud_in_salt
   topics/idem_cloud
   topics/acct_plugins
   topics/acct_backends
   topics/acct_cli
   topics/tool_plugins
   topics/exec_modules
   topics/state_modules
   topics/idem_cli
   topics/pop_isms
   topics/contracts
   topics/recursive_contracts
   topics/common_pitfalls
   releases/*


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
